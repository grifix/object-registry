<?php

declare(strict_types=1);

namespace Grifix\ObjectRegistry\Tests;

use Grifix\ObjectRegistry\Exceptions\ObjectAlreadyExistsException;
use Grifix\ObjectRegistry\Exceptions\ObjectDoesNotExistException;
use Grifix\ObjectRegistry\MemoryObjectRegistry;
use Grifix\ObjectRegistry\Tests\Dummies\Circle;
use Grifix\ObjectRegistry\Tests\Dummies\Square;
use PHPUnit\Framework\TestCase;

final class MemoryObjectRegistryTest extends TestCase
{
    public function testItAddAndGets(): void
    {
        $registry = new MemoryObjectRegistry();
        $circle = new Circle();
        $square = new Square();
        $registry->addObject($circle, 'alpha');
        $registry->addObject($circle, 'beta');
        $registry->addObject($square, 'alpha');
        $registry->addObject($square, 'beta');

        self::assertSame($circle, $registry->getObject(Circle::class, 'alpha'));
        self::assertSame($circle, $registry->getObject(Circle::class, 'beta'));
        self::assertSame($square, $registry->getObject(Square::class, 'alpha'));
        self::assertSame($square, $registry->getObject(Square::class, 'beta'));
    }

    public function testItFailsToGetObject(): void
    {
        $registry = new MemoryObjectRegistry();
        $this->expectException(ObjectDoesNotExistException::class);
        $this->expectExceptionMessage('Object [Grifix\ObjectRegistry\Tests\Dummies\Circle] with alias [circle] does not exist in the registry!');
        $registry->getObject(Circle::class, 'circle');
    }

    public function testItFailsToAddObject(): void
    {
        $registry = new MemoryObjectRegistry();
        $registry->addObject(new Circle(), 'circle');
        $this->expectException(ObjectAlreadyExistsException::class);
        $this->expectExceptionMessage('Object [Grifix\ObjectRegistry\Tests\Dummies\Circle] with alias [circle] already exist in the registry!');
        $registry->addObject(new Circle(), 'circle');
    }

}
