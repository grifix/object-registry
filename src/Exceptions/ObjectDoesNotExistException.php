<?php

declare(strict_types=1);

namespace Grifix\ObjectRegistry\Exceptions;

final class ObjectDoesNotExistException extends \Exception
{

    public function __construct(string $class, string $alias)
    {
        parent::__construct(
            sprintf('Object [%s] with alias [%s] does not exist in the registry!', $class, $alias)
        );
    }
}
