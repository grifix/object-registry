<?php

declare(strict_types=1);

namespace Grifix\ObjectRegistry;

use Grifix\ObjectRegistry\Exceptions\ObjectAlreadyExistsException;
use Grifix\ObjectRegistry\Exceptions\ObjectDoesNotExistException;

interface ObjectRegistryInterface
{
    /**
     * @throws ObjectAlreadyExistsException
     */
    public function addObject(object $object, string $alias): void;

    /**
     * @template T
     *
     * @param class-string<T> $class
     *
     * @return T
     * @throws ObjectDoesNotExistException
     */
    public function getObject(string $class, string $alias): mixed;
}
