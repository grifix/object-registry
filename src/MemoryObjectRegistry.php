<?php

declare(strict_types=1);

namespace Grifix\ObjectRegistry;

use Grifix\ObjectRegistry\Exceptions\ObjectAlreadyExistsException;
use Grifix\ObjectRegistry\Exceptions\ObjectDoesNotExistException;

final class MemoryObjectRegistry implements ObjectRegistryInterface
{
    private array $objects = [];

    public function addObject(object $object, string $alias): void
    {
        if (!isset($this->objects[$object::class])) {
            $this->objects[$object::class] = [];
        }
        if (array_key_exists($alias, $this->objects[$object::class])) {
            throw new ObjectAlreadyExistsException($object::class, $alias);
        }
        $this->objects[$object::class][$alias] = $object;
    }

    public function getObject(string $class, string $alias): mixed
    {
        if (!isset($this->objects[$class]) || !isset($this->objects[$class][$alias])) {
            throw new ObjectDoesNotExistException($class, $alias);
        }
        return $this->objects[$class][$alias];
    }
}
